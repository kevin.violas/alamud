from .event import Event3


class CutWithEvent(Event3):
    NAME = "cut-with"

    def perform(self):
        self.inform("cut-with")
