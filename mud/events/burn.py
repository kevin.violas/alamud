from .event import Event3


class BurnWithEvent(Event3):
    NAME = "burn-with"

    def perform(self):
        self.inform("burn-with")
