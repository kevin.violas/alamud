from .event import Event3

class ScreamEvent(Event3):
    NAME = "scream"

    def perform(self):
        self.add_prop("screamed-"+self.object2)
        self.inform("scream")
