from .event import Event1

class ListenEvent(Event1):
    NAME = "listen"

    def perform(self):
       self.buffer_clear()
       if not self.actor.music_activated():
           self.actor.music_activation()
           self.buffer_inform("listen.activated")
           self.actor.send_result(self.buffer_get())
       else:
           self.buffer_inform("listen.fail")
           self.actor.send_result(self.buffer_get())

class StopListenEvent(Event1): 
     name = "stoplisten" 

     def perform(self):     
       self.buffer_clear()
       if self.actor.music_activated():
           self.actor.music_desactivation()
           self.buffer_inform("stoplisten.desactivated")
           self.actor.send_result(self.buffer_get())
       else:
           self.buffer_inform("stoplisten.fail")
           self.actor.send_result(self.buffer_get())          
