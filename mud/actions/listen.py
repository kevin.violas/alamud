from .action import Action1
from mud.events import ListenEvent, StopListenEvent

class ListenAction(Action1):
    EVENT = ListenEvent
    ACTION = "listen"

class StopListenAction(Action1):
    EVENT = StopListenEvent
    ACTION = "stoplisten"
