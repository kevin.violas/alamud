from .action import Action2
from mud.events import ReadEvent


class ReadAction(Action2):
    EVENT = ReadEvent
    ACTION = "Read"
    RESOLVE_OBJECT = "resolve_for_operate"