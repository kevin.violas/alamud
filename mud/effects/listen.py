from .effect import Effect1
from mud.events import ListenEvent, StopListenEvent

class ListenEffect(Effect1):
    EVENT = ListenEvent

class StopListenEffect(Effect1):
    EVENT = StopListenEvent
