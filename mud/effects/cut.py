from .effect import Effect3
from mud.events import CutWithEvent

class CutWithEffect(Effect3):
    EVENT = CutWithEvent
